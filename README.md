# HTML2Pango

Small rust lib to parse simple html tags and convert to pango. This lib also
converts raw links to http links and sanitizes the message to avoid not wanted tags.

The current state of the library is Alpha as it's a proof of concept for mapping
a subset of html to pango markup.

This code was inside [Fractal][fractal] project to parse matrix.org messages that contains
links and make that drawable in a GtkLabel. We decided to move to this repo to
be able to extend and use in other projects like [Hammond][hammond].

[fractal]: https://gitlab.gnome.org/World/fractal
[hammond]: https://gitlab.gnome.org/alatiera/hammond